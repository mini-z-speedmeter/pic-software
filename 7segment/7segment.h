/* 
 * File:   7segment.h
 * Author: Pila
 *
 * Created on 2 janvier 2014, 17:04
 */



#ifndef SEGMENT_H
#define	SEGMENT_H
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!                                       !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!               SETTINGS                !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!     edit this part to match your      !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!        hardware configuration         !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!                                       !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


#include "../config.h"
#include <stdint.h>

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!                                       !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!                HEADERS                !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!          do not edit below            !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!                                       !!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//Init the I/Os
void sevenSegInit();

//Set the position of the decimal point
void sevenSegSetDecimalPointPosition(uint8_t position);

//display a number between 0 and 999 (included).
void sevenSegSetNumber(uint16_t valueToDisplay);

//display a number by setting each of its digits
void sevenSegSetNumberByDigits(uint8_t hundredDigit, uint8_t decadeDigit, uint8_t unitDigit);

//allow to set directly the state of each segment. The common anode / common cathode
//stuff is handled, so any bit set to 1 will always turn the corresponding segment on.
void sevenSegSetRawValue(uint8_t hundred, uint8_t decade, uint8_t unit);

//this function handle the display. Each call handles one digit. You must call it
//more than 90 times per second to avoid visible flicker of the display. In most
//programs, this function is called by an interruption routine. A >=100Hz call rate gives
//a very nice, flicker-free display.
void sevenSegUpdateDisplay(void);


#endif
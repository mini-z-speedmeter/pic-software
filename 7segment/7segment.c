#include <xc.h>
#include "7segment.h"

#define sevenSegDigitOn 0
#define sevenSegDigitOff 1

#define adaptToPort(x)    ~((x&0b11000000)|((x>>1)&0b00011111));

const uint8_t sevenSegDigitArray[] = {0b00100001,0b10101111,0b00110010,0b00100110,0b10001100,0b01100100,0b01100000,0b00101111,0b00100000,0b00100100};

static uint8_t sevenSegUnitValue;
static uint8_t sevenSegDecadeValue;
static uint8_t sevenSegHundredValue;
static uint8_t sevenSegDecimalPointPosition = 0;

void sevenSegInit(){
    sevenSegSetRawValue(0x00,0x00,0x00);
    sevenSegOutputTris=0x00;
    sevenSegUnitCommonTris=0;
    sevenSegDecadeCommonTris=0;
    sevenSegHundredCommonTris=0;
    sevenSegDecimalPointTris=0;
}

void sevenSegSetDecimalPointPosition(uint8_t position){
    sevenSegDecimalPointPosition=position;
}

void sevenSegSetNumber(uint16_t valueToDisplay){
    if(valueToDisplay>999)valueToDisplay=999;
    sevenSegUnitValue=sevenSegDigitArray[valueToDisplay%10];
    valueToDisplay/=10;
    sevenSegDecadeValue=sevenSegDigitArray[valueToDisplay%10];
    sevenSegHundredValue=sevenSegDigitArray[valueToDisplay/10];
}

void sevenSegSetNumberByDigits(uint8_t hundredDigit, uint8_t decadeDigit, uint8_t unitDigit){
    sevenSegHundredValue=sevenSegDigitArray[hundredDigit%10];
    sevenSegDecadeValue=sevenSegDigitArray[decadeDigit%10];
    sevenSegUnitValue=sevenSegDigitArray[unitDigit%10];
}

void sevenSegSetRawValue(uint8_t hundred, uint8_t decade, uint8_t unit){
    sevenSegHundredValue=adaptToPort(hundred);
    sevenSegDecadeValue=adaptToPort(decade);
    sevenSegUnitValue=adaptToPort(unit);
}

void sevenSegUpdateDisplay(void){
    static uint8_t unitDigitBeingDisplayed = 0xff;
    sevenSegDecimalPointPin=!(sevenSegDecimalPointPosition==unitDigitBeingDisplayed);
    switch(unitDigitBeingDisplayed){
        case 0:
            sevenSegHundredCommonPin=sevenSegDigitOff;
            sevenSegOutputPort=sevenSegUnitValue;
            sevenSegUnitCommonPin=sevenSegDigitOn;
            unitDigitBeingDisplayed=1;
            break;
            
        case 1:
            sevenSegUnitCommonPin=sevenSegDigitOff;
            sevenSegOutputPort=sevenSegDecadeValue;
            sevenSegDecadeCommonPin=sevenSegDigitOn;
            unitDigitBeingDisplayed=2;
            break;

        case 2:
            sevenSegDecadeCommonPin=sevenSegDigitOff;
            sevenSegOutputPort=sevenSegHundredValue;
            sevenSegHundredCommonPin=sevenSegDigitOn;
            unitDigitBeingDisplayed=0;
            break;

        default:
            sevenSegUnitCommonPin=sevenSegDigitOff;
            sevenSegDecadeCommonPin=sevenSegDigitOff;
            sevenSegHundredCommonPin=sevenSegDigitOff;
            unitDigitBeingDisplayed=0;
            break;
    }
}

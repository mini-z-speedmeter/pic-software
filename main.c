/* 
 * File:   main.c
 * Author: Pila
 *
 * Created on 2 septembre 2014, 21:07
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <xc.h>

#include "7segment/7segment.h"
#include "softuart/softuart.h"
#include "config.h"


#pragma config FOSC = INTOSCIO  // Oscillator Selection bits (INTOSC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF       // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is MCLR)
#pragma config BOREN = ON      // Brown-out Detect Enable bit (BOD disabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB4/PGM pin has digital I/O function, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define STATE_INIT              0
#define STATE_READY             1
#define STATE_COUNTING          2
#define STATE_DISPLAYING        3

#define resetCounters()         TMR1L=0;TMR1H=0;overflowCounter=0
#define CALCULATION_CONSTANT    ((uint32_t)TICK_TO_KMH_RATIO)*((uint32_t)DISTANCE_BETWEEN_SENSORS)


volatile uint8_t currentState;
volatile uint8_t overflowCounter;

char helloMsgUart[] = HELLO_MSG_UART;
char slowMsgUart[] = SLOW_MSG_UART;
char errorMsgUart[] = ERROR_MSG_UART;
char lowBatMsgUart[] = LOWBAT_MSG_UART;

void startIrSignalGeneration();
void initSensors();
void initTimers();
void processAndDisplaySpeed();

void main() {
    currentState = STATE_INIT;

    //init hardware
    CMCON = 0x07;
    sevenSegInit();
    initTimers();
    initSensors();

    //init communication
    softUartInit();

    //Display Battery before powering up leds
    softUartPrint(lowBatMsgUart);
    softUartEndLine();
    sevenSegSetRawValue(LOWBAT_MSG_DISPLAY);
    __delay_ms(BATTERY_DELAY);

    //init timers
    startIrSignalGeneration();
    __delay_ms(BATTERY_DELAY);

    //getting ready and displaying hello message
    softUartPrint(helloMsgUart);
    softUartEndLine();
    sevenSegSetRawValue(HELLO_MSG_DISPLAY);
    __delay_ms(DISPLAY_DELAY);
    currentState = STATE_READY;

    for (;;) {
        switch (currentState) {
            case STATE_COUNTING:
                //blanking display
                sevenSegSetDecimalPointPosition(0xff);
                sevenSegSetRawValue(COUNTING_MSG_DISPLAY);
                break;

            case STATE_DISPLAYING:
                //processing and displaying speed
                processAndDisplaySpeed();
                //get ready for next loop and wait some time
                resetCounters();
                __delay_ms(DISPLAY_DELAY);
                currentState = STATE_READY;
                break;

        }
    }
}

void initSensors(){
    sensor1Tris = 1;
    sensor2Tris = 1;
    PORTB |= (0x01 << sensor1PinNumber | 0x01 << sensor2PinNumber);
}

void initTimers() {
    TMR0 = 0;
    T1CON = 0b00110000; //TMR1 use internal clock, 1:8 prescaler
    OPTION_REG = 0b01000011; //TMR0 use internal clock, 1:16 prescaler
    INTCON = 0b11101000; //peripheral, TMR0 and PORTB interrupts enabled
    PIE1 = 0b00000001; //TMR1 interrupts only;
    resetCounters();
}

void startIrSignalGeneration() {
    PR2 = PR2_VALUE; //generating signal...
    CCPR1L = PR2_VALUE >> 1; //... with 50% duty cycle
    CCP1CON = 0b00001100; //configuring CCP as PWM
    TRISBbits.TRISB3 = 0; //enabling output
    TMR2 = 0; //clearing TMR2
    T2CON = 0b00000100; //starting TMR2 with 1:1 prescaler
}

void processAndDisplaySpeed() {
    if (overflowCounter >= TMR1_MAX_OVERFLOW) {
        softUartPrint(slowMsgUart);
        softUartEndLine();
        sevenSegSetRawValue(SLOW_MSG_DISPLAY);
    } else {
        uint32_t timeElapsedBetweenSensors = overflowCounter;
        timeElapsedBetweenSensors <<= 8;
        timeElapsedBetweenSensors |= TMR1H;
        timeElapsedBetweenSensors <<= 8;
        timeElapsedBetweenSensors |= TMR1L;
        uint32_t speed = CALCULATION_CONSTANT / timeElapsedBetweenSensors; // s = d / t
        if (speed < 10000) {
            uint16_t speedToDisplay = speed;
            //sending speed through uart
            softUartTxByte(0x30 | (speedToDisplay / 1000));
            speedToDisplay %= 1000;
            softUartTxByte(0x30 | (speedToDisplay / 100));
            speedToDisplay %= 100;
            softUartTxByte(0x30 | (speedToDisplay / 10));
            softUartTxByte('.');
            softUartTxByte(0x30 | (speedToDisplay %= 10));
            softUartEndLine();

            //displaying
            speedToDisplay = speed;
            if (speedToDisplay < 1000) {
                sevenSegSetNumber(speedToDisplay);
                sevenSegSetDecimalPointPosition(1);

            } else {
                sevenSegSetNumber(speedToDisplay / 10);
                sevenSegSetDecimalPointPosition(0);
            }
        } else {
            softUartPrint(errorMsgUart);
            softUartEndLine();
            sevenSegSetRawValue(ERROR_MSG_DISPLAY);
        }
    }
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!                                                          !!!!!!!!!!
//!!!!!!!!!!                       INTERRUPTS                         !!!!!!!!!!
//!!!!!!!!!!                                                          !!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void onSensorTriggered();
void onTmr1Overflow();

void interrupt interruptFunction() {
    if (INTCONbits.RBIF) {  //Sensors
        onSensorTriggered();
        INTCONbits.RBIF = 0;
    }
    if (PIR1bits.TMR1IF) {  //Timer
        onTmr1Overflow();
        PIR1bits.TMR1IF = 0;
    }
    if (INTCONbits.T0IF) {  //Display refresh
        sevenSegUpdateDisplay();
        INTCONbits.T0IF = 0;
    }
}

void onSensorTriggered() {
    static uint8_t prevPortBState = ((!SENSOR_ACTIVE) << sensor1PinNumber) | ((!SENSOR_ACTIVE) << sensor2PinNumber);
    uint8_t currPortBState = PORTB;
    uint8_t portDiff = prevPortBState ^ currPortBState;

    //First sensor triggered
    if ((currentState == STATE_READY) && (((currPortBState >> sensor1PinNumber)&0x01) == SENSOR_ACTIVE) && (portDiff & (0x01 << sensor1PinNumber))) {
        T1CONbits.TMR1ON = 1;
        currentState = STATE_COUNTING;
    }

    //Second sensor triggered
    else if ((currentState == STATE_COUNTING) && (((currPortBState >> sensor2PinNumber)&0x01) == SENSOR_ACTIVE) && (portDiff & (0x01 << sensor2PinNumber))) {
        T1CONbits.TMR1ON = 0;
        currentState = STATE_DISPLAYING;
    }
    prevPortBState = currPortBState;
}

void onTmr1Overflow() {
    overflowCounter++;
    if (overflowCounter == TMR1_MAX_OVERFLOW) {    //Timeout
        T1CONbits.TMR1ON = 0;
        currentState = STATE_DISPLAYING;
    }
}
/* 
 * File:   config.h
 * Author: Pila
 *
 * Created on 13 septembre 2014, 00:36
 */

#ifndef CONFIG_H
#define	CONFIG_H

//STRINGS

#define HELLO_MSG_DISPLAY           0b00001010,0b01111010,0b01110110
#define SLOW_MSG_DISPLAY            0b10110110,0b00011000,0b00111010
#define ERROR_MSG_DISPLAY           0b10011110,0b00001010,0b00001010
#define LOWBAT_MSG_DISPLAY          0b00111110,0b11101110,0b00011110
#define COUNTING_MSG_DISPLAY        0b00000010,0b00000010,0b00000010
#define HELLO_MSG_UART              "SpeedMeter v1\r\nready"
#define SLOW_MSG_UART               "slow"
#define ERROR_MSG_UART              "error"
#define LOWBAT_MSG_UART             "low battery"

//DELAYS

#define DISPLAY_DELAY               750
#define BATTERY_DELAY               500

//ALGORITHM CONSTANTS

#define TMR1_MAX_OVERFLOW           4
#define DISTANCE_BETWEEN_SENSORS    895         // (mm)
#define TICK_TO_KMH_RATIO           4500        // 3600 * 10 / 8

//SENSOR CONFIG

#define sensor1PinNumber            7
#define sensor1Tris                 TRISBbits.TRISB7
#define sensor2PinNumber            6
#define sensor2Tris                 TRISBbits.TRISB6
#define SENSOR_ACTIVE               1

//DISPLAY CONFIG

#define sevenSegUnitCommonPin       PORTBbits.RB0
#define sevenSegUnitCommonTris      TRISBbits.TRISB0
#define sevenSegDecadeCommonPin     PORTBbits.RB1
#define sevenSegDecadeCommonTris    TRISBbits.TRISB1
#define sevenSegHundredCommonPin    PORTBbits.RB2
#define sevenSegHundredCommonTris   TRISBbits.TRISB2
#define sevenSegOutputPort          PORTA
#define sevenSegOutputTris          TRISA
#define sevenSegDecimalPointPin     PORTBbits.RB4
#define sevenSegDecimalPointTris    TRISBbits.TRISB4

//UART CONFIG

#define softUartTxPin               PORTBbits.RB5
#define softUartTxTris              TRISBbits.TRISB5
#define softUartHighLevel           1
#define softUartLowLevel            0
#define softUartBaudrate            9600

//using internal oscillator
#define PR2_VALUE                   26
#define _XTAL_FREQ                  4000000

#endif	/* CONFIG_H */


/* 
 * File:   softuart.h
 * Author: Pila
 *
 * Created on 24 septembre 2014, 22:12
 */

#ifndef SOFTUART_H
#define	SOFTUART_H

void softUartInit();
void softUartTxByte(unsigned char b);
void softUartPrint(char* Text);
void softUartEndLine();


#endif	/* SOFTUART_H */


// This file is based on SoftWare Uart Lib by Nart Schinackow

#include "softuart.h"
#include "../config.h"
#include <xc.h>

#define BITPERIOD               ((1000000/softUartBaudrate)-2)
void softUartInit(){
    softUartTxPin=1;
    softUartTxTris=0;
    softUartTxPin=1;
    __delay_us(BITPERIOD);
}

void softUartTxByte(unsigned char b){
    GIE=0;
    unsigned char mask;
    softUartTxPin = softUartLowLevel;
    __delay_us(BITPERIOD-5);

    for (mask=0x01;mask!=0;mask=mask<<1){
        if(b&mask) softUartTxPin = softUartHighLevel;
        else softUartTxPin = softUartLowLevel;
        __delay_us(BITPERIOD-17);
    }
    softUartTxPin = softUartHighLevel;
    __delay_us(BITPERIOD);
    GIE=1;
}

void softUartPrint(char* Text)
{
 while(*Text)softUartTxByte(*Text++);
}

void softUartEndLine(){
    softUartTxByte('\r');softUartTxByte('\n');
}